

/* Validaciones */
$(function(){

    $("#rut_errormsg").hide();
    $("#name_errormsg").hide();
    $("#fecnac_errormsg").hide();
    $("#mail_errormsg").hide();
    $("#fono_errormsg").hide();

    var err_rut = false;
    var err_name = false;
    var err_fecnac = false;
    var err_mail = false;
    var err_fono = false;

    $("#idRut").focusout(function(){
        check_rut();
    });
    $("#idNombre").focusout(function(){
        check_name();
    });
    $("#idFecNac").focusout(function(){
        check_fecnac();
    });
    $("#idMail").focusout(function(){
        check_mail();
    });
    $("#idFono").focusout(function(){
        check_fono();
    });

    /* validar Rut */
    function check_rut(){
        var rut = new RegExp(/^[0-9.]+-[kK0-9]{2,4}$/i);

        if (rut.test($("#idRut").val())) {
            $("#rut_errormsg").hide();
        } else {
            $("#rut_errormsg").html("Rut invalido.");
            $("#rut_errormsg").show();
            err_rut = true;
        }
    }

    /* validar Nombre Completo */
    function check_name(){
        var name = new RegExp(/^[a-zA-Z]{2,4}$/i);
            
        if (name.test($("#idNombre").val())) {
            $("#name_errormsg").hide();
        } else {
            $("#name_errormsg").html("Nombre invalido.");
            $("#name_errormsg").show();
            err_name = true;
        }
    }

    /* validar Fecha de nacimiento */ /* F A L T A */
    function check_fecnac(){

    }

    /* validar Mail */
    function check_mail(){
		var mail = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
	
		if(mail.test($("#idMail").val())) {
			$("#mail_errormsg").hide();
		} else {
			$("#mail_errormsg").html("Direccion de correo invalida.");
			$("#mail_errormsg").show();
			err_mail = true;
		}
    }

    /* validar telefono */
    function check_fono(){
        var fono = new RegExp(/^[+0-9]+[0-9]{2,4}$/i);

        if(fono.test($("#idFono").val())) {
			$("#fono_errormsg").hide();
		} else {
			$("#fono_errormsg").html("Telefono invalido.");
			$("#fono_errormsg").show();
			err_fono = true;
		}
    }


	$("#form").submit(function() {
											
		err_rut = false;
		err_name =false;
		err_fecnac = false;
		err_mail =false;
		err_fono =false;
                                            
        check_rut();
        check_name();
        check_fecnac();
        check_mail();
        check_fono();

		if(err_rut == false && err_name == false && err_fecnac == false && err_mail == false && err_fono == false) {
			return true;
		} else {
			return false;	
		}

	});



});


