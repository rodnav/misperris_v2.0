


/* jquery Dropdowns */
$("#idRegion").change(function() {

	var $dropdown = $(this);

	$.getJSON("js/regionciudad.json", function(data) {
	
		var key = $dropdown.val();
		var vals = [];
                            
        switch (key) {
            case 'AP':
                vals = data.AP.split(",");  
                break;
            case 'TA':
                vals = data.TA.split(",");  
                break;
            case 'AN':
                vals = data.AN.split(",");  
                break;
            case 'AT':
                vals = data.AT.split(",");  
                break;
            case 'CO':
                vals = data.CO.split(",");  
                break;
            case 'VP':
                vals = data.VP.split(",");  
                break;
            case 'BO':
                vals = data.BO.split(",");  
                break;
            case 'MA':
                vals = data.MA.split(",");  
                break;
            case 'BB':
                vals = data.BB.split(",");  
                break;
            case 'AR':
                vals = data.AR.split(",");  
                break;
            case 'RI':
                vals = data.RI.split(",");  
                break;
            case 'LA':
                vals = data.LA.split(",");  
                break;
            case 'AY':
                vals = data.AY.split(",");  
                break;
            case 'MG':
                vals = data.MG.split(",");  
                break;
            case 'ST':
                vals = data.ST.split(",");  
                break;
/*             default:
                break; */
        }


		var $idCiudad = $("#idCiudad");
		$idCiudad.empty();
		$.each(vals, function(index, value) {
			$idCiudad.append("<option>" + value + "</option>");
		});

	});
});












/* Tarapacá
Antofagasta
Atacama
Coquimbo
Valparaíso
Libertador General Bernardo O’Higgins
Maule
Bío-Bío
La Araucanía
Los Lagos
Aysén del General Carlos Ibáñez del Campo
Magallanes y Antártica Chilena
Región Metropolitana de Santiago
Los Ríos
Arica y Parinacota
Ñuble */